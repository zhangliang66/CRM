import Vue from 'vue'
import Router from 'vue-router'

import HpQa from '@/components/hp/HpQa'
import EditClass from '@/components/hp/EditClass'
import RepairControl from '@/components/hp/RepairControl'
import RepairWork from '@/components/hp/RepairWork'
import Custom from '@/components/hp/Custom'
import CustomerService from '@/components/hp/CustomerService'
import CustomerCare from '@/components/hp/CustomerCare'
import CustomerComplain from '@/components/hp/CustomerComplain'
import ManageRepairWork from '@/components/hp/ManageRepairWork'

import Main from '@/components/Main'
import Login from '@/components/Login'

import firstPage from '@/components/firstPage'
import detailQuotationView from '@/components/zl/detailQuotationView'
import Loudou from '@/components/zl/Loudou'
import salechanceview from '@/components/zl/salechanceview'
import customerdemandView from '@/components/zl/customerdemandView'
import competitorView from '@/components/zl/competitorView'
import quotationView from '@/components/zl/quotationView'
import solutionView from '@/components/zl/solutionView'
import DetailQuotation from '@/components/zl/DetailQuotation'
import Solution from '@/components/zl/Solution'
import Quotation from '@/components/zl/Quotation'
import Competitor from '@/components/zl/Competitor'
import FindSaleChance from '@/components/zl/FindSaleChance'
import Department2User from '@/components/whl/Department2User'
import CustomerDemand from '@/components/zl/CustomerDemand'

import CusMain from '@/components/hxy/CusMain'
import ConMain from '@/components/hxy/ConMain'
import Memorial from '@/components/hxy/Memorial'
import CusTLog from '@/components/hxy/CusTLog'
import CollInfo from '@/components/hxy/CollInfo'
import Collect from '@/components/hxy/Collect'
import CusMeger from '@/components/hxy/CusMeger'
import CusView from '@/components/hxy/CusView'
import CusTransfer from '@/components/hxy/CusTransfer'
import CusMegerLog from '@/components/hxy/CusMegerLog'
import CustomerExcel from '@/components/hxy/CustomerExcel'

import Order from '@/components/cy/Order'
import OrderView from '@/components/cy/OrderView'
import OrDetail from '@/components/cy/OrDetail'
import Invoice from '@/components/cy/Invoice'
import Store from '@/components/cy/Store'
import ReturnPlan from '@/components/cy/ReturnPlan'
import ReturnRecord from '@/components/cy/ReturnRecord'
import Back from '@/components/cy/Back'
import BackGood from '@/components/cy/BackGood'
import SendGood from '@/components/cy/SendGood'
import OrSend from '@/components/cy/OrSend'
import WareHouse from '@/components/cy/WareHouse'
import WareBuy from '@/components/cy/WareBuy'
import BuyGood from '@/components/cy/BuyGood'
import WareIn from '@/components/cy/WareIn'
import InGood from '@/components/cy/InGood'
import WareOut from '@/components/cy/WareOut'
import OutGood from '@/components/cy/OutGood'

import ZcInseruser from '@/components/whl/ZcInseruser'
import RolesDept from '@/components/whl/RolesDept'
import Product from '@/components/whl/Product'
import ProductType from '@/components/whl/ProductType'
import UserSet from '@/components/whl/UserSet'

Vue.use(Router)



export default new Router({
	routes: [
		{
			path: '/',
			name: 'Login',
			component: Login,
		},
		{
			path: '/main',
			name: 'main',
			component: Main,
			redirect:"/main/firstPage",
			children: [
				{
				path: 'firstPage',
				name: 'firstPage',
				component: firstPage
				},
				{
				path: 'cus',
				name: 'cus',
				component: CusMain
				},
				{
				path: 'con',
				name: 'con',
				component: ConMain
				},
				{
				path: 'memo',
				name: 'memo',
				component: Memorial
				},
				{
				path: 'log',
				name: 'log',
				component: CusTLog
				},
				{
				path: 'info',
				name: 'info',
				redirect:'Loudou',
				component: CollInfo
				},
				{
				path: 'cusMeger',
				name: 'cusMeger',
				component: CusMeger
				},
				{
				path: 'cusTransfer',
				name: 'cusTransfer',
				component: CusTransfer
				},
				{
				path: 'customerExcel',
				name: 'customerExcel',
				component: CustomerExcel,
				},{
					path:'Loudou',
					name:'Loudou',
					component:Loudou
				},
				{
					path:'FindSaleChance',
					name:'FindSaleChance',
					component:FindSaleChance
				},
				{
					path:'Solution',
					name:'Solution',
					component:Solution
				}
				,
				{
					path:'DetailQuotation',
					name:'DetailQuotation',
					component:DetailQuotation
				},
				{
					path:'Quotation',
					name:'Quotation',
					component:Quotation
				},
				{
					path:'Competitor',
					name:'Competitor',
					component:Competitor
				},{
					path:'CustomerDemand',
					name:'CustomerDemand',
					component:CustomerDemand
				},
				{
					path:'/department2User',
					name:'department2User',
					component: Department2User
				},
				 {
					path:'hpQa',
					name:'hpQa',
					component:HpQa
				},
				{
					path: 'editClass',
					name: 'editClass',
					component:EditClass
				},
				{
					path: 'repairControl',
					name: 'repairControl',
					component:RepairControl
				},
				{
					path: 'repairWork',
					name: 'repairWork',
					component:RepairWork
				},
				{
					path: 'custom',
					name: 'custom',
					component:Custom
				},
				{
					path: 'customerService',
					name: 'customerService',
					component:CustomerService
				},
				{
					path: 'customerCare',
					name: 'customerCare',
					component:CustomerCare
				},
				{
					path: 'customerComplain',
					name: 'customerComplain',
					component:CustomerComplain
				},
				
				{
					path:'Order',
					name:'Order',
					component:Order
				},
				{
					path:'OrDetail',
					name:'OrDetail',
					component:OrDetail
				},
				{
					path:'Invoice',
					name:'Invoice',
					component:Invoice
				},
				{
					path:'Store',
					name:'Store',
					component:Store
				},
				{
					path:'ReturnPlan',
					name:'ReturnPlan',
					component:ReturnPlan
				},
				{
					path:'ReturnRecord',
					name:'ReturnRecord',
					component:ReturnRecord
				},
				{
					path:'Back',
					name:'Back',
					component:Back
				},
				{
					path:'BackGood',
					name:'BackGood',
					component:BackGood
				},
				{
					path:'SendGood',
					name:'SendGood',
					component:SendGood
				},
				{
					path:'OrSend',
					name:'OrSend',
					component:OrSend
				},
				{
					path:'WareHouse',
					name:'WareHouse',
					component:WareHouse
				},
				{
					path:'WareBuy',
					name:'WareBuy',
					component:WareBuy
				},
				{
					path:'BuyGood',
					name:'BuyGood',
					component:BuyGood
				},
				{
					path:'WareIn',
					name:'WareIn',
					component:WareIn
				},
				{
					path:'InGood',
					name:'InGood',
					component:InGood
				},
				{
					path:'WareOut',
					name:'WareOut',
					component:WareOut
				},
				{
					path:'OutGood',
					name:'OutGood',
					component:OutGood
				},
				{
					path:'/rolesDept',
					name:'rolesDept',
					component:RolesDept
				},
				{
					path:'/product',
					name:'product',
					component:Product
				},
				{
					path:'/productType',
					name:'productType',
					component:ProductType
				},{
					path:'/userSet',
					name:'userSet',
					component:UserSet
				}
			]
		},
		{
			path:'/zcInseruser',
			name:'zcInseruser',
			component:ZcInseruser,
		},
		{
			path: '/coll',
			name: 'coll',
			component: Collect
		},
		{
			path: '/cusView',
			name: 'cusView',
			component: CusView
		},
		{
			path: '/cusMegerLog',
			name: 'cusMegerLog',
			component: CusMegerLog
		},
		{
			path: '/salechanceview',
			name: 'salechanceview',
			component: salechanceview
		},
		{
			path: '/competitorView',
			name: 'competitorView',
			component: competitorView
		},
		{
			path: '/quotationView',
			name: 'quotationView',
			component: quotationView
		},
		{
			path: '/solutionView',
			name: 'solutionView',
			component: solutionView
		},
		{
			path: '/customerdemandView',
			name: 'customerdemandView',
			component: customerdemandView
		},
		{
			path: '/manageRepairWork',
			name: 'manageRepairWork',
			component: ManageRepairWork,
			},{
			path: '/detailQuotationView',
			name: 'detailQuotationView',
			component: detailQuotationView
		},
		{
			path: '/OrderView',
			name: 'OrderView',
			component: OrderView
		}
	]
})
