import * as API from '../'

export default{
	findAllBg: params => {
		return API.GET("selectAllBg",params);
	},
	deleteOneBg: params => {
		return API.DELETE("deleteOneBg",params);
	},
	addBg: params => {
		return API.POST("addBuyGood",params);
	},
	findOneBg: params => {
		return API.GET("selectOneBg",params);
	},
	deleteBgs: params => {
		return API.POST("deleteBgs",params);
	}
}