import * as API from '../'

export default{
	findAllBack:params => {
		return API.GET("selectAllBack", params);
	},
	deleteOneBack: params => {
		return API.DELETE("deleteBack",params);
	},
	addBack: params => {
		return API.POST("addBack",params);
	},
	findOneBack: params => {
		return API.GET("selectOneBack",params);
	},
	deleteBacks: params => {
		return API.POST("deleteBacks",params);
	},
	findBackByType: params =>{
		return API.GET("selectBackByType",params);
	}
}