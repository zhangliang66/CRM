import * as API from '../'

export default{
	findAllSd: params => {
		return API.GET("selectAllSd",params);
	},
	deleteOneSd: params => {
		return API.DELETE("deleteOneSd",params);
	},
	addSendGood: params => {
		return API.POST("addSendGood",params);
	},
	findOneSd: params => {
		return API.GET("selectOneSd",params);
	},
	deleteSds: params => {
		return API.POST("deleteSds",params);
	},
	findSdByType: params =>{
		return API.GET("selectSdByType",params);
	}
}