import * as API from '../'

export default{
	findRecordByPage: params => {
		return API.GET("selectAllRecord",params);
	},
	deleteOneRecord: params => {
		return API.DELETE("deleteRecord",params);
	},
	addRecord: params => {
		return API.POST("addReturnRecord",params);
	},
	findOneRecord: params => {
		return API.GET("selectOneRecord",params);
	},
	deleteRecords: params => {
		return API.POST("deleteReturnRecords",params);
	},
	findRecordByType: params =>{
		return API.GET("selectRecordByWays",params);
	}
}