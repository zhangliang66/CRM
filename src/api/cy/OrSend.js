import * as API from '../'

export default{
	findAllOsd: params => {
		return API.GET("selectAllOsd",params);
	},
	deleteOneOsd: params => {
		return API.DELETE("deleteOneOsd",params);
	},
	addOsd: params => {
		return API.POST("addOrSend",params);
	},
	findOneOsd: params => {
		return API.GET("selectOneOsd",params);
	},
	deleteOsds: params => {
		return API.POST("deleteWos",params);
	}
}