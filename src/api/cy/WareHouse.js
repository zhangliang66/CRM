import * as API from '../'

export default{
	findAllWh: params => {
		return API.GET("selectAllWh",params);
	},
	deleteOneWh: params => {
		return API.DELETE("deleteOneWo",params);
	},
	addWareHouse: params => {
		return API.POST("addWareOut",params);
	},
	findOneWh: params => {
		return API.GET("selectOneWh",params);
	},
	deleteWhs: params => {
		return API.POST("deleteWos",params);
	},
	findWhByType: params =>{
		return API.GET("selectWoByStatus",params);
	}
}