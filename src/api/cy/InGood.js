import * as API from '../'

export default{
	findAllIg: params => {
		return API.GET("selectAllIg",params);
	},
	deleteOneIg: params => {
		return API.DELETE("deleteOneIg",params);
	},
	addIg: params => {
		return API.POST("addInGood",params);
	},
	findOneIg: params => {
		return API.GET("selectOneIg",params);
	},
	deleteIgs: params => {
		return API.POST("deleteBgs",params);
	}
}