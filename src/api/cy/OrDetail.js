import * as API from '../'

export default{
	findAllOde: params => {
		return API.GET("selectAllOde",params);
	},
	deleteOneOde: params => {
		return API.DELETE("deleteOneOde",params);
	},
	addOde: params => {
		return API.POST("addOrDetail",params);
	},
	findOneOde: params => {
		return API.GET("selectOneOde",params);
	},
	deleteOdes: params => {
		return API.POST("deleteWos",params);
	},
	getPro: params => {
		return API.GET("getPro",params);
	}
}