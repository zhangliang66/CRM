import * as API from '../'

export default{
	findOrByPage:params => {
		return API.GET("findAllOrder", params);
	},
	deleteOr:params => {
		return API.DELETE("deleteOrder",params);
	},
	addOrder:params => {
		return API.POST("addOrder",params);
	},
	load: params => {
		return API.GET("findOneOrder",params);
	},
	findOneOr:params => {
		return API.GET("findOneOrder",params);
	},
	deleteOrs:params => {
		return API.POST("deleteOrders",params)
	},
	findOrByType:params => {
		return API.GET("findOrderByType",params);
	},
	findOdeForOr: params => {
		return API.GET("selectOneByOrder",params);
	},
	getOrder: params => {
		return API.GET("getOrder",params);
	},
	findOneByNo:params => {
		return API.GET("findOneByNo",params);
	}
	
}