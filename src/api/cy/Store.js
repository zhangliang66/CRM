import * as API from '../'

export default{
	findStByPage:params => {
		return API.GET("selectAllSt", params);
	},
	deleteOneSt: params => {
		return API.DELETE("deleteOneSt",params);
	},
	addStore: params => {
		return API.POST("addStore",params);
	},
	findOneSt: params => {
		return API.GET("selectOneSt",params);
	},
	deleteSts: params => {
		return API.POST("deleteStores",params);
	},
	findStByType: params =>{
		return API.GET("selectStByType",params);
	}
}