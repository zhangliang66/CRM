import * as API from '../'

export default{
	findAllOg: params => {
		return API.GET("selectAllOg",params);
	},
	deleteOneOg: params => {
		return API.DELETE("deleteOneOg",params);
	},
	addOg: params => {
		return API.POST("addOutGood",params);
	},
	findOneOg: params => {
		return API.GET("selectOneOg",params);
	},
	deleteOgs: params => {
		return API.POST("deleteOgs",params);
	}
}