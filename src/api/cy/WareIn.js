import * as API from '../'

export default{
	findAllWi: params => {
		return API.GET("selectAllWi",params);
	},
	deleteOneWi: params => {
		return API.DELETE("deleteOneWi",params);
	},
	addWareIn: params => {
		return API.POST("addWareIn",params);
	},
	findOneWi: params => {
		return API.GET("selectOneWi",params);
	},
	deleteWis: params => {
		return API.POST("deleteWis",params);
	},
	findWiByType: params =>{
		return API.GET("selectWiByStatus",params);
	}
}