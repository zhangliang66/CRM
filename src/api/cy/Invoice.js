import * as API from '../'

export default{
	findAllInv: params => {
		return API.GET("selectAllInv",params);
	},
	deleteOneInv: params => {
		return API.DELETE("deleteOneInv",params);
	},
	addInvoice: params => {
		return API.POST("addInvoice",params);
	},
	findOneInv: params => {
		return API.GET("selectOneInv",params);
	},
	deleteInvs: params => {
		return API.POST("deleteInvs",params);
	},
	findInvByType: params =>{
		return API.GET("selectInvByType",params);
	}
}