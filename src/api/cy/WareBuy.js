import * as API from '../'

export default{
	findAllWb: params => {
		return API.GET("selectAllWb",params);
	},
	deleteOneWb: params => {
		return API.DELETE("deleteOneWb",params);
	},
	addWareBuy: params => {
		return API.POST("addWareBuy",params);
	},
	findOneWb: params => {
		return API.GET("selectOneWb",params);
	},
	deleteWbs: params => {
		return API.POST("deleteWbs",params);
	},
	findWbByType: params =>{
		return API.GET("selectWbByStatus",params);
	}
}