import * as API from '../'

export default{
	findAllWo: params => {
		return API.GET("selectAllWo",params);
	},
	deleteOneWo: params => {
		return API.DELETE("deleteOneWo",params);
	},
	addWareOut: params => {
		return API.POST("addWareOut",params);
	},
	findOneWo: params => {
		return API.GET("selectOneWo",params);
	},
	deleteWos: params => {
		return API.POST("deleteWos",params);
	},
	findWoByType: params =>{
		return API.GET("selectWoByStatus",params);
	}
}