import * as API from '../'

export default{
	findRnpByPage: params => {
		return API.GET("selectAllRnp",params);
	},
	deleteOneRnp: params => {
		return API.DELETE("deleteOneRnp",params);
	},
	addRnp: params => {
		return API.POST("addReturnPlan",params);
	},
	findOneRnp: params => {
		return API.GET("selectOneRnp",params);
	},
	deleteRnps: params => {
		return API.POST("deleteReturnPlans",params);
	},
	findRnpByType: params =>{
		return API.GET("selectRnpByType",params);
	}
}