import * as API from '../'

export default{
	findAllBkg: params => {
		return API.GET("selectAllBkg",params);
	},
	deleteOneBkg: params => {
		return API.DELETE("deleteBackGood",params);
	},
	addBkg: params => {
		return API.POST("addBackGood",params);
	},
	findOneBkg: params => {
		return API.GET("selectOneBkg",params);
	},
	deleteBkgs: params => {
		return API.POST("deleteBkgs",params);
	}
}