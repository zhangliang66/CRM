import * as API from '../' 
export default{
	/*查询出没有删除的QA*/
	queryAllQa:params=>{
		return API.GET("queryQAByPage",params);
	},
	/* 新增QA记录 */
	addQAData:params=>{
		return API.POST("addAndupdateQA",params);
	},
	/*根据ID删除QA*/
	deleteQAByID:params=>{
		return API.DELETE("delete_qa",params);
	},
	/* 查询分类 */
	queryTotalClass:params=>{
		return API.GET("findTotalclass",params);
	},
	/* 分类查询 */
	queryByClass:params=>{
		return API.GET("queryQAByClass",params);
	},
	/* 问题搜索 */
	queryByQuestion:params=>{
		return API.GET("querybyquestion",params);
	}
}