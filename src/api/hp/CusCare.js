import * as API from '../'

export default{
	/* 分页查询所有的投诉 */
	queryCusCareByPage:params=>{
		return API.GET("queryCusCare_byPage",params);
	},
	/* 逻辑删除一条数据 */
	deleteCusCare:params=>{
		return API.DELETE("deleteCusCare",params);
	},
	/* 批量删除 */
	deleteByBatchCusCare:params=>{
		return API.POST("deleteCuscareList",params);
	},
	/* 添加和修改关怀 */
	addAndUpdateCusCare:params=>{
		return API.POST("addAndUpCusCare",params);
	},
	/* 加载分类 */
	queryTotalClass:params=>{
		return API.GET("findTotalclass",params);
	},
	/* 分类查询 */
	queryByCareType:params=>{
		return API.GET("queryCusCare_byType",params);
	},
	/* 问题查询 */
	queryByCareTitle:params=>{
		return API.GET("queryCusCare_byTitle",params);
	},
}