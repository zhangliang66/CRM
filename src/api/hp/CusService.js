import * as API from '../'

export default{
	/* 分页查询所有的投诉 */
	queryCusServiceByPage:params=>{
		return API.GET("queryCustomerService_byPage",params);
	},
	/* 逻辑删除一条数据 */
	deleteCusService:params=>{
		return API.DELETE("deleteCustomerService",params);
	},
	/* 批量删除数据 */
	deleteBatchCusService:params=>{
		return API.POST("deleteCusServiecList",params);
	},
	/* 添加和修改关怀 */
	addAndUpdateCusService:params=>{
		return API.POST("addCustomerService",params);
	}
	
}