import * as API from '../'

export default{
	/* 分页查询所有的投诉 */
	queryAllCpByPage:params=>{
		return API.GET("queryCusComplain_ByPage",params);
	},
	/* 逻辑删除一条数据 */
	deleteComplain:params=>{
		return API.DELETE("deleteCusComplain",params);
	},
	/* 批量删除一条数据 */
	deleteByBatchList:params=>{
		return API.POST("deleteBatch_list",params);
	},
	/* 添加和修改一条客户投诉 */
	addAndUpdateCusComplain:params=>{
		return API.POST("addAndUpCusComplain",params);
	},
	/* 圆饼图统计查询 */
	queryByPie:params=>{
		return API.GET("queryCusCompByPie",params);
	},
	/* 柱状图图统计查询 */
	queryByBar:params=>{
		return API.GET("queryCusCompByBar",params);
	},
	/* 折线图统计查询 */
	queryByLine:params=>{
		return API.GET("queryCusCompByLine",params);
	},
	/* 查询分类 */
	queryComplainType:params=>{
		return  API.GET("findTotalclass",params);
	},
	/* 高级查询 */
	queryByHighContion:params=>{
		return API.POST("queryHighCondition",params);
	},
	/* 加载首位接待人 */
	queryFirstPeople:params=>{
		return API.GET("queryFirstName",params);
	}
}