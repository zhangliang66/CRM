import * as API from '../'

export default{
	findAllLog: params => {
		return API.GET("cusTLog_list", params);
	},
	findLogUser: params => {
		return API.GET("log_user_list", params);
	}
}