import * as API from '../'

export default{
	findAllInfo: params => {
		return API.GET("cInfo_list", params);
	},
	findNewFive: params => {
		return API.GET("cInfo_newFive", params);
	}
}