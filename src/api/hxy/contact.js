import * as API from '../'

export default{
	findAllCon: params => {
		return API.POST("con_list", params);
	},
	deleteOneCon: params => {
		return API.DELETE("con_deleteOne", params);
	},
	findByCusId: params => {
		return API.GET("con_list_byCusId", params);
	},
	loadOneCon : params => {
		return API.GET("con_load", params);
	},
	saveCon : params => {
		return API.POST("con_save", params);
	},
	deleteManyCon : params => {
		return API.POST("con_deleteMany", params);
	}
	
}