import * as API from '../'

export default{
	memoA: params => {
		return API.GET("memo_nearA_list", params);
	},
	findAllMemo: params => {
		return API.POST("memo_list", params);
	},
	deleteOneMemo: params => {
		return API.DELETE("memo_deleteOne", params);
	},
	loadOneMemo: params => {
		return API.GET("memo_load", params);
	},
	saveMemo: params => {
		return API.POST("memo_save", params);
	},
	deleteManyMemo: params => {
		return API.POST("memo_deleteMany", params);
	}
	
	
}