import * as API from '../'

export default{
	findAllColl: params => {
		return API.GET("collect_list", params);
	}
}