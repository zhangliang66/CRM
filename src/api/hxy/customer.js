import * as API from '../'

export default{
	//条件高级查询
	findAllCus: params => {
		return API.POST("cus_list", params);
	},
	//无条件，不分页查询
	findAllNopage: params => {
		return API.GET("cus_list_nopage", params);
	},
	delOneCus: params => {
		return API.DELETE("cus_delOne", params);
	},
	addOneCus: params => {
		return API.POST("cus_add", params);
	},
	loadOneCus: params => {
		return API.GET("cus_load", params);
	},
	updateOneCus: params => {
		return API.POST("cus_update", params);
	},
	getData: params => {
		return API.GET("cus_getData", params);
	},
	//客户合并
	cusMeger: params => {
		return API.GET("cus_meger", params);
	},
	//客户合并日志
	cusMegerOLog: params => {
		return API.GET("cusMeger_oLog", params);
	},
	test: params => {
		return API.POST("test", params);
	},
	excelCustomer: params => {
		return API.POST("excelCustomer", params);
	},
	transCus: params => {
		return API.POST("cus_transfer", params);
	},
	loadOneCusDetail: params => {
		return API.GET("cus_findOneDetail", params);
	}, 
	//客户视图时间线
	cusTimeLine: params => {
		return API.GET("cus_timeLine", params);
	},
	//三一客
	cusThree: params => {
		return API.GET("cus_three", params);
	},
	insertThreeOne: params => {
		return API.POST("insertThreeOne", params);
	}
}