import * as API from '../'

export default{
	delUserRoles: params => {
		return API.POST("delUserRoles",params);
	},
	selectProca: params => {
		return API.GET("selectProca",params);
	},
	insertProCa:params => {
		return API.GET("insertProCa",params);
	},
	updataProCa: params => {
		return API.GET("updataProCa",params);
	},
	deleteProca: params => {
		return API.GET("deleteProca",params);
	},
	queryqbcp: params => {
		return API.GET("queryqbcp",params);
	},
	finfyAll: params => {
		return API.GET("finfyAll",params);
	},
	querylzuser: params => {
		return API.GET("querylzuser",params);
	},
	insertProduct: params => {
		return API.POST("insertProduct",params);
		},
	deleteProduct: params => {
		return API.GET("deleteProduct",params);
	},
	queryidpro: params => {
		return API.GET("queryidpro",params);
	},
	finallidUser: params =>{
		return API.GET("finallidUser",params);
	},
	qkSession: params => {
		return API.GET("qkSession",params);
	},
	updateProduct: params => {
		return API.POST("updateProduct",params);
	},
	queryuidfmk: params => {
		return API.GET("queryuidfmk",params);
	},
	qrypwd: params => {
		return API.GET("qrypwd",params);
	},
	updatepwd: params => {
		return API.GET("updatepwd",params);
	},
	insertPhoto:params => {
		return API.POST("insertPhoto",params);
	},
	queryphuid: params => {
		return API.GET("queryphuid",params);
	}
}
