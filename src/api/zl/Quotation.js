import * as API from '../'
export default{
	findAllQuotation: params => {
		return API.GET("findAllQuotation", params);
	},
	deleteQuotation: params => {
		return API.DELETE("deleteOneQuotation", params);
	},
	addAndUpdateQuotation: params => {
		return API.POST("addAndUpdateQuotation", params);
	},
	load: params => {
		return API.GET("loadQuotation", params);
	},
	deleteQuotations: params => {
		return API.POST("deleteManyQuotation", params);
	},
	findBy: params => {
		return API.POST("findBy", params);
	},findByQuoId: params => {
		return API.GET("findByQuoId", params);
	},findProByNameOrCode: params => {
		return API.GET("findProByNameOrCode", params);
	},addDQuoList: params => {
		return API.POST("addDQuoList", params);
	},findAllDquo: params => {
		return API.GET("findAllDquo", params);
	},deleteDquo: params => {
		return API.GET("deleteDquo", params);
	}
}