import * as API from '../'
export default{
	findAllCD: params => {
		return API.GET("findAllCDemand", params);
	},
	deleteCD: params => {
		return API.DELETE("deleteOneCD", params);
	},
	addAndUpdateCD: params => {
		return API.POST("addAndUpdateCD", params);
	},
	load: params => {
		return API.GET("loadCD", params);
	},
	deletecds: params => {
		return API.POST("deleteManyCD", params);
	},findBy: params => {
		return API.POST("findCustomerdemandBy", params);
	}
}