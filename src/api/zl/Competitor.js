import * as API from '../'
export default{
	findAllCompetitor: params => {
		return API.GET("findAllCompetitor", params);
	},
	deleteCompetitor: params => {
		return API.DELETE("deleteOneCompetitor", params);
	},
	addAndUpdateCompetitor: params => {
		return API.POST("addAndUpdateCompetitor", params);
	},
	load: params => {
		return API.GET("loadCompetitor", params);
	},
	deleteCompetitors: params => {
		return API.POST("deleteManyCompetitor", params);
	},findCompetitorBy: params => {
		return API.POST("findCompetitorBy", params);
	}
}