import * as API from '../'
export default{
	findAllSol: params => {
		return API.GET("findAllSol", params);
	},
	deleteSol: params => {
		return API.DELETE("deleteOneSol", params);
	},
	addAndUpdateSol: params => {
		return API.POST("addAndUpdateSol", params);
	},
	load: params => {
		return API.GET("loadSol", params);
	},
	deleteSols: params => {
		return API.POST("deleteManySol", params);
	}
}